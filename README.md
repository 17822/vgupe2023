# VGUPE2023

VGU Programming Exercise

## Description:
[Module description](VGU-Module-Description.pdf)

## Team 1: Online Library Management
### Members
 1. 17822 Lý Nam Sơn (Leader)
 2. 17675 Nguyễn Văn Khánh Nghi
 3. 17700 Nguyễn Phạm Kiên Trung
 4. 17137 Hồ Vĩnh Hào
 5. 17159 Lê Hoàng Thiên Quang
 6. 17544 Viên Kim Lân
 7. 17851 Huỳnh Minh Triết


## Team 2: [LAPP](https://gitlab.com/galvdat/vgu_tinyprojects/pe2023/vgupe2023_team2)
### Members:
 1. Phan Chí Thọ (17232)
 2. Trần Nguyễn Minh Quân (17640)
 3. Lê Hoàng Kim Thanh (18047)(Leader)
 4. Nguyễn Lê Anh Quân (18875)
 5. Phạm Nguyễn Đan Quỳnh (17937) 
 6. Lê Hoàng Đăng Nguyên (17028)
 7. Vương Khánh Linh (18070)
 8. Hoàng Minh Thông (17995)


## Team 3: Online Library Management
### Members:
 1. Tra Dang Khoa- 13789744 (team leader)
 2. Son Nguyen Thanh - 13789908
 3. Dong Do Dinh - 13789722
 4. Quang Nguyen Huu Minh - 13789726
 5. Mai Trọng Nhân - 13794792
 6. Tran Hoang Kim - 13796562
 7. Võ Nguyễn Minh Duy (He) - 13796504
 8. Chau Truong Vinh Hoang - @16076 - User ID: 5928255


 ## Team 4: Online Fastfood Ordering
 ### Members:
 1. Phạm Lê Hoàng Duy - 17590 - @17590 (leader)
 2. Nguyễn Phan Trúc Ly - 18715 - @18715
 3. Võ Đăng Khoa - 18955 - @GoodGuyBin
 4. Truong Nguyen Thien An - 16772 - Thrasos728
 5. Hoang An-15693- @HoangAn3011
 6. Hoang An CSE2019 - 15693- 
 7. Trần Trung Nghĩa - 17420 - @trungnghia17420 (guest)
 8. Trần Đăng Khoa - 15713 - @FinnK30062001 (guest)
 9. Vũ Duy Thành - 17712 - @rickyvu2311 (guest)


 ## Team 5: [Online Movie Management System](https://docs.google.com/document/d/1spvPDBs7l7YFBG1TLc5Ze3q9_Y5r2wQpQleSmZRkkUA/edit)
 ### Members:
 1. 18840 Hà Quách Phú Thành (Leader)
 2. 18973 Nguyễn Xuân Khang
 3. 18770 Thái Quang Nam
 4. 18230 Nguyễn Khắc HoàngHoàng
 5. 18334 Phạm Hoàng Việt
 6. 17197 Trần Ngọc Duy Chương
 7. 18691 Nguyễn Ngọc Vĩnh
 8. 17434 Lê Duy

 ## Team 6: Medical record management system
 ### Members:
 1. 17885 - Nguyễn Hoàng Nam - @Visionman123 (Project Manager)
 2. 17116 - Vĩnh Nguyễn Phước Bảo Minh - @minhnguyen1312
 3. 17647 - Mai Nguyễn Vy - @vymai
 4. 17835 - Phạm Như Ngọc - @17835
 5. 17975 - Tăng Thành Đạt - @DatLit1
 6. 18148 - Nguyễn Ngô Minh Trí -  @Yxxeratia
 7. 17422 - Phạm Trương Nhật Nguyên - @TiChuts
 8. 17306 - Trần Minh Ngọc - @MichelleTran02

 ## Team 7: Online Bus Ticket Booking
 ### Members:
 1. Vũ Hoàng Tuấn Anh (18812) (Leader)
 2. Bá Nguyễn Quốc Anh (17965)
 3. Trần Kim Hoàn (18810)
 4. Trương Hoàng Nam (17273)
 5. Nguyễn Hoàng Hải Nam (17035)

 ## Team 8: Online Sneaker Store
 ### Members:
 1. Trà Nguyên Ngọc - 17156 - Leader
 2. Lương Gia Bảo - 17535
 3. Nguyễn Đức Thọ - 14841 
 4. Lê Trần Gia Thịnh - 18819 
 5. Trần Minh Hiếu - 17242 
 6. Nguyễn Huỳnh Trung Hiếu - 17727
 7. Phùng Hoàng Hiệp - 17237
 8. Nguyễn Duy Nhật - 18937

 ## Team 9: online library management
 ### Members:
 1. Nguyen Khoa - 14820 - @khoas (leader)
 2. Nguyen Van Khanh - 17096 - @Nomoreherozz
 3. Trang Huu Hoang Do - 16014 - @CL0u9
 4. Huynh Duong Khai - 17529 - @r0s3nv3nsy
 5. Hoang The Vinh - 17466 - @thevinhhoang02
 6. Thai Nguyen Hoang Nam - 17466

 ## Team 10: Game Store Application
 ### Members:
 1. Vo Nguyen Duy Bach - 16770 - @167701
 2. Nguyen Gia Bao - 16885 - @168851
 3. Hoang Duc Hung - 16207 - @162071
 4. Hoang Ngoc Anh Khoi - 15900 - @159001
 5. Pham Phuc Khoa - 16765 - @167651 (inccorect)
 6. Le Duc Minh - 16669 - @169991
 7. Nguyen Tuan Ngoc - 15809 - @tuanngocfun
 8. Nguyen Tran Duc Cao -  @141171

## Team 11: Museum Management System
### Members:
 1. Trần Minh Hoàng - 17401 - @JimHoth (Leader)
 2. Huỳnh Nguyễn Chí Hiếu - 17523 - @CheeseHu
 3. Hà Cẩm Tú - 17362 - @ellieryus
 4. Nguyễn Bảo Hoàng Chương - 17098 - @DyzilestarHorches
 5. Nguyễn Quốc Huy - 17409 - @nguyenhuy200258
 
 ## Team 12: Library System Management
### Members:
 1. Ngo Dinh Anh Khoa - 17557 - @17557 (Leader)
 2. Tran Quang Minh - 17061 - @17061
 3. Phi Dinh Van Toan - 17431 - @LoveLess12
 4. Nguyen Vu Doanh Khoa - 17539 - @ngvudoanhkhoa15102002
 5. Truong Van Quang - 10421050 - @Der_Entwickler
 6. Nguyen Truong Duy - 17455 - @nguyentruongduy